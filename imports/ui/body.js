import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';


import './body.html';
import './file-upload.js';

console.log("body.js called");

// My own custom Handlebar helpers
Handlebars.registerHelper('getExtension', function(passedString) {
    var s = passedString.split('.').pop();
    return new Handlebars.SafeString(s)
});

Handlebars.registerHelper('cleanTitle', function(passedString) {
    passedString = passedString.replace(/\.[^/.]+$/, "");
    return new Handlebars.SafeString(passedString)
});

Template.body.helpers({
	getAllFiles(){
		// console.log(localStorage.getItem('sortByFavourites'));
		// Check if user is filtering the list by favourites
		if (localStorage.getItem('sortByFavourites') == 'favourites'){
			var allFileIDs = [];
			var allFiles = [];
			UserFiles.find({userID:Meteor.userId()}).forEach( function(file) {
				allFileIDs.push(file.fileID);
			});

			for (var i=0; i<allFileIDs.length; i++){
				var file = Files.findOne({_id:allFileIDs[i]});
				allFiles.push(file);
			}
			return allFiles;
		}else{
			return Files.find({});
		}
	},

	fileInUsersFavourites(fileID){
		var fileExists = UserFiles.findOne({fileID:fileID, userID:Meteor.userId()});
		if (fileExists){
			return true;
		}
		return false;
	},

  	// admin() {
  	// 	return Roles.userIsInRole(Meteor.userId(), 'admin');
  	// },

});

// var list_of_fields = [];
var HashMap = require('hashmap');
var kv_form_input = new HashMap();
var current_file = "";

Template.body.events({
  	'click .logout': () => {
  		AccountsTemplates.logout();
  	},

  	'click .deleteFile': (event) => {
  		// console.log(event);
  		if(Roles.userIsInRole(Meteor.userId(), 'admin')){
	  		var fileID = event.target.parentElement.children[2].children["0"].innerText;
	  		var file = Files.findOne({_id:fileID});
	  		Meteor.call('deleteFile', file._id);
  		}else{
  			console.log('Cannot perform this action. User is not an Admin');
  		}
  	},

  	'click .sortByFav':() => {
  		localStorage.setItem('sortByFavourites', 'favourites');
	},

	'click .sortByAll':() => {
		localStorage.setItem('sortByFavourites', 'all');
	},

  	'click .addToFavourites': (event) => {
		var fileID = event.toElement.parentElement.parentElement.children[1].children["0"].innerText;
		Meteor.call('addFileToUsersFavourites', fileID, Meteor.userId());
	},

	'click .removeFromFavourites': (event) => {
		var fileID = event.toElement.parentElement.parentElement.children[1].children["0"].innerText;
		Meteor.call('removeFileToUsersFavourites', fileID, Meteor.userId());
	},

  	'click .display_form': (event) => {
  		// start with empty hashmap
  		kv_form_input.clear();

		// Get value from form element
		// console.log(event);
		var templateImgSrc = event.currentTarget.children[1].currentSrc; // image path
		var templateDes = event.currentTarget.children[2].children[1].innerText; // description
		var templateTitle = event.currentTarget.children[2].children["0"].innerText; // title
		
		var fileID = event.currentTarget.children["0"].innerHTML;
		// console.log(fileID);

		var f = Files.find({_id:fileID}).fetch();
		// var f = Meteor.call('getFileByName', text);
		current_file = f;
		// console.log(f); // display file in browser console

		// Printing all fields to browser console
		// console.log(f);
		// for (var i=0; i<f[0].fields.length; i++){
		// 	console.log(f[0].fields[i]);
		// }

		// remove form if it already exists
		var formExists = document.getElementById("temp_pdf_gen_form");
		if (typeof(formExists) != 'undefined' && formExists != null){
		  formExists.remove();
		}

		document.getElementById("templateHeader").style.visibility = "hidden";
		document.getElementById("templatesRow").style.visibility = "hidden";
		document.getElementById("formDiv").style.display = "block";

		document.getElementById("templateImage").setAttribute("src", templateImgSrc);
		document.getElementById("templateTitle").innerText = templateTitle;
		document.getElementById("templateDescription").innerText = templateDes;

		// Start constructing the form
		var form = document.createElement("form");
		form.setAttribute('id',"temp_pdf_gen_form");
		form.setAttribute('method',"");
		form.setAttribute('action',"");
		form.setAttribute('style',"margin:20px; padding-left:25px; border-left:1px solid #ECECEC;");

		for (var i=0; i<f[0].fields.length; i++){
			var field = f[0].fields[i];
			// Get field type
			var fieldType = String(field).match(/{{[a-zA-Z ]*\|/g)[0].replace(/{{|\|/g,""); 
			// console.log(fieldType);

			field = field.replace('{{',''); // remove double left brackets
			field = field.replace('}}',''); // remove doulbe right brackets
			field = field.replace(/[a-zA-Z ]*\|[ ]*/g, ""); // remove the type
			
			if (!kv_form_input.has(field)){
				kv_form_input.set(field,"");
			 	var label = field.charAt(0).toUpperCase() + field.slice(1);
				console.log(label);

				var el = document.createElement("input"); //input element, text
				// el.setAttribute('type',"text");
				el.setAttribute('type',fieldType); // set input type using type of field
				el.setAttribute('style',"margin-bottom:15px");
				el.setAttribute('class',"form-control");
				el.setAttribute('placeholder',label);
				el.setAttribute('name',field);
				form.appendChild(el);

				linebreak = document.createElement("br");
				form.appendChild(linebreak);
			}else{
				console.log("Skipping: "+field);
			}
		}

		var s = document.createElement("input"); //input element, Submit button
		s.setAttribute('type',"submit");
		s.setAttribute('value',"Generate PDF");
		s.setAttribute('style',"width:100%");
		s.setAttribute('class','btn btn-primary at-btn submit generate_pdf closeForm downloadPDF');
		s.innerHTML = "Generate PDF";
		form.appendChild(s);
		document.getElementById('pdfGenForm').appendChild(form);
  	},

  	'click .closeForm': (event) => {
  		document.getElementById("templatesRow").style.visibility = "visible";
  		document.getElementById("templateHeader").style.visibility = "visible";
		document.getElementById("formDiv").style.display = "none";

		var templateForm = document.getElementById("temp_pdf_gen_form");
		templateForm.remove();
  	},

  	'click .generate_pdf': function (event) {
    	event.preventDefault();

		// Getting the values from the form
		var form = event.target.form.elements;
		for (input_value in form){
			if (kv_form_input.has(input_value)){
				kv_form_input.set(input_value, form[input_value].value);	
				// console.log("Key:"+input_value+" Value:"+form[input_value].value);
			}
		}
		Meteor.call('pdf_gen', kv_form_input, current_file, Meteor.userId());
    }, 

    'click .downloadPDF': function (event) {
    	event.preventDefault();
    	setTimeout(function(){
		    var file = UserFiles.findOne({userID:Meteor.userId(), deleteID:"TempFile"});
	    	var FileSaver = require('file-saver');
			var blob = new Blob([file.data], {type: "application/pdf"});
			FileSaver.saveAs(blob, file.fileName.substr(0, file.fileName.lastIndexOf('.')));
			Meteor.call('deleteGeneratedPDF', Meteor.userId(), "TempFile");
		},2000);
    	
    }
});











