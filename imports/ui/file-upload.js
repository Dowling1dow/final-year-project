import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
// import { Files } from '../api/files.js';
import './file-upload.html';

console.log("file-upload.js called");

Template.fileUpload.events({
	'change input': function(event,template){ 
		// console.log(event.delegateTarget.children[1].children[1].value);
		// console.log(event);

		// var fileDescription = event.delegateTarget.children[1].children[1].value;
		var fileDescription = event.delegateTarget.children["0"].children["0"].value;
		console.log(fileDescription);

	    var file = event.target.files[0]; //assuming 1 file only
	    // console.log(file);
	    if (!file) return;
	    
	    var filename = file.name;
	    var file_extension = filename.substr(filename.lastIndexOf('.')+1)

	    var reader = new FileReader(); // create a reader according to HTML5 File API
	    reader.readAsArrayBuffer(file); // read the file as arraybuffer
	    reader.onload = function(event){
			var buffer = new Uint8Array(reader.result); // convert to binary
			Meteor.call('saveFile', buffer, file.name, fileDescription);
	    }

	    document.getElementById("file-description").value = ""; // clear textarea
	    document.getElementById("upload-label").setAttribute('disabled', 'disabled'); // set upload label back to disabled
	},
});
