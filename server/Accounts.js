var postSignUp = function(userId, info){
	console.log(userId);
	console.log(info);

	// Comment this line if you uncomment the other 
	Roles.addUsersToRoles(userId, ['normal-user']);

	// Uncomment line below to make every new user an admin
	// Roles.addUsersToRoles(userId, ['admin']);
}

AccountsTemplates.configure({
	postSignUpHook: postSignUp
});