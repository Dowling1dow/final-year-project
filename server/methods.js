
Meteor.methods({
    'getFiles': function(){
      return Files.find();
    },

    'getFileByName': function(filename){
      return Files.find({name:filename}).fetch();
    },

    'deleteFile': function(fileID){
      return Files.remove(fileID);
    },

    'addFileToUsersFavourites': function(fileID, userID){
      UserFiles.insert({ fileID:fileID, userID:userID });
    },

    'removeFileToUsersFavourites': function(fileID, userID){
      UserFiles.remove({ fileID:fileID, userID:userID });
    },

    'deleteGeneratedPDF': function(userId, deleteId){
      UserFiles.remove({ userID:userId, deleteID:deleteId });
    },

    'saveFile': function(buffer, filename, fileDescription){
      /*
        This method takes in the file and scans the file for fields.
        It adds those fields to a list and stores it with the file
        inside of the 'Files' collection.
      */

      var zip = require("jszip");
      var file_extension = filename.split('.').pop();
      var txtEdit = "";
      var list_of_fields = [];
      var docContent = []; 
      var images = [];
      var isSlideShow = false;
      var isXlsx = false;
      var isDocx = false;
      var hasImages = false;
      var error = true;

      zip.loadAsync(buffer).then(function(content) {
        switch (file_extension) {
          case 'docx':
            isDocx = true;
            // Seach for images here
            docContent.push(content.files["word/document.xml"].async('text'));
            docContent.push(content.files["word/_rels/document.xml.rels"].async('text'));
            // NOTE: Only supporting JPEG images for now, PDFKit only supports jpg and png
            var j=1;
            if (content.files["word/media/image1.jpg"]){
              hasImages = true;
              console.log("Media folder exists");
              while (content.files["word/media/image"+j+".jpg"]){
                var pic = content.files["word/media/image"+j+".jpg"].async('uint8array');
                images.push(pic); // push the image uint8array
                images.push("image"+j+".jpg"); // push the image name
                j++;
              }
              console.log("Found "+images.length+" images");
              docContent.push(images);
            }
            return docContent;

          case 'odt':
            // Search for images here
            docContent.push(content.files["content.xml"].async('text'));
            return docContent;

          case 'ods':
            docContent.push(content.files["content.xml"].async('text'));
            return docContent;

          case 'xlsx':
            // Only handling 1 sheet for now (these are tabs in excel)
            isXlsx = true;
            docContent.push(content.files["xl/worksheets/sheet1.xml"].async('text'));
            docContent.push(content.files["xl/sharedStrings.xml"].async('text'));
            return docContent;

          case 'pptx':
            isSlideShow = true;
            slides = [];
            rel_files = [];
            var i = 1;
            // If there is a slide to get, push it to slides list
            // Get the slide rels file also
            while (content.files["ppt/slides/slide"+i+".xml"]){
              // console.log("Found Slide"+i);
              var file = content.files["ppt/slides/slide"+i+".xml"].async('text');
              var rel_file = content.files["ppt/slides/_rels/slide"+i+".xml.rels"].async('text');
              slides.push(file);
              rel_files.push(rel_file);
              i++;
            }
            console.log("All slides found");
            docContent.push(slides);

            // NOTE: Only supporting JPEG images for now
            var j=0, k=0;
            if (content.files["ppt/media/image00.jpg"]){
              hasImages = true;
              console.log("Media folder exists");
              while (content.files["ppt/media/image"+j+""+k+".jpg"]){
                // var pic = content.files["ppt/media/image"+j+""+k+".jpg"];
                var pic = content.files["ppt/media/image"+j+""+k+".jpg"].async('uint8array');
                images.push(pic); // push the image uint8array
                images.push("image"+j+""+k+".jpg"); // push the image name
                k++;
                if (k>9){
                  k=0;
                  j++;
                }
              }
              console.log("Found "+images.length+" images");
              docContent.push(images);
            }

            // Pushing rel files here so its at the end of the list
            docContent.push(rel_files);
            return docContent;

          default:
            console.log("Format Not Supported");
            break;
        }

      }).then(function (docContent) {
        if (isSlideShow){
            var txtEdit = [];

            Promise.all(docContent[0]).then(function (docs){
              for (var i=0; i<docs.length; i++){
                txtEdit.push(docs[i]);
                var docFields = getFields(docs[i]);
                console.log("Printing list_of_fields array after reading slide "+(i+1));
                list_of_fields = list_of_fields.concat(docFields);
                console.log(list_of_fields);
              }
              console.log("\n\nReturning this list:");
              console.log(list_of_fields);

              Promise.all(docContent[2]).then(function(rels){
               if (hasImages){
                  Promise.all(docContent[1]).then(function(images){
                    Files.insert({ data:buffer, name:filename, description:fileDescription, fields:list_of_fields, fileToEdit:txtEdit, media:images, relFiles:rels});
                  });
                }else{
                  var empty = [];
                  Files.insert({ data:buffer, name:filename, description:fileDescription, fields:list_of_fields, fileToEdit:txtEdit, media:empty, relFiles:rels});
                }
              });
            });

        }else if(isXlsx){
          console.log("Saving XLSX file");
          Promise.all(docContent).then(function(xlsxFiles){
            console.log(xlsxFiles);
            Files.insert({ data:buffer, name:filename, description:fileDescription, fields:getFields(xlsxFiles[1]), fileToEdit:xlsxFiles[0], sharedStrings:xlsxFiles[1] }); 
          });

        }else if(isDocx){
          console.log("Saving DOCX file");
          Promise.all(docContent).then(function(docFiles){
            if (hasImages){
              console.log("IMAGE FILE: "+docContent[2]);
              Promise.all(docFiles[2]).then(function(images){
                Files.insert({ data:buffer, name:filename, description:fileDescription, fields:getFields(docFiles[0]), fileToEdit:docFiles[0], media:images, relFile:docFiles[1] }); 
              });
            }else{
              var empty = [];
              // Check getDocxFields description to understand the use of getDocxFields
              // Files.insert({ data:buffer, name:filename, description:fileDescription, fields:getDocxFields(docFiles[0]), fileToEdit:docFiles[0], media:empty, relFile:docFiles[1] }); 
              Files.insert({ data:buffer, name:filename, description:fileDescription, fields:getFields(docFiles[0]), fileToEdit:docFiles[0], media:empty, relFile:docFiles[1] }); 
            }
          });

        }else{
          console.log(docContent);
          Promise.all(docContent).then(function(textContent){
            console.log(getFields(textContent));
            Files.insert({ data:buffer, name:filename, description:fileDescription, fields:getFields(textContent), fileToEdit:textContent });
          });
        }
      });
    },

    'pdf_gen': function(kv_form_input, current_file, userId){

      var HashMap = require('hashmap');
      var docContent = current_file[0].fileToEdit; // File(s) containing textual content
      var images = current_file[0].media; // working with pptx only
      var filename = current_file[0].name

      var isList = true;
      // Check to see if fileToEdit is an array or single string
      // If it's an array then we have a slideshow file
      if(typeof docContent === 'string'){ isList = false; }

      var file_extension = current_file[0].name.split('.').pop();
      var fieldsMap = new HashMap(kv_form_input);

      docContent = replaceFields(isList, docContent, fieldsMap);

      // At this point now, we have the textual content of the document
      // with all the fields replaced with their values

      zip = require("jszip");
      var fs = require('fs');
      var PDFDocument = require('pdfkit');

      var paragraphs = [];
      switch (file_extension) {
        case 'docx':
          var relFile = current_file[0].relFile;
          generateDocxPDF(fs, PDFDocument, docContent, relFile, images, filename, userId);
          break;

        case 'odt':
          paragraphs = odtTextContent(docContent);
          generateOdtPDF(fs, PDFDocument, paragraphs, userId, filename);
          break;

        case 'ods':
          generateOdsPDF(fs, PDFDocument, docContent, filename, userId);
          break;

        case 'pptx':
          // At this point docContent is an array of slides
          var relFiles = current_file[0].relFiles;
          generatePptxPDF(fs, PDFDocument, docContent, images, relFiles, filename, userId);
          break;

        case 'xlsx':
          generateXlsxPDF(fs, PDFDocument, docContent, current_file[0].sharedStrings, filename, userId);
          break;
      }
    }

});

/*************************************
  Replacing the fields in the text with form input values
**************************************/
function replaceFields(isList, docContent, fieldsMap){
  // var re = /{{[a-zA-Z0-9_ ]*}}/g; // for non-typed arrays
  var re = /{{[a-zA-Z ]*\|[a-zA-Z0-9_ ]*}}/g; // For typed fields
  var m;
  console.log("Replacing fields:");
  console.log(isList);
  if (isList){
    for (var i=0; i<docContent.length; i++){
      do {
        m = re.exec(docContent[i]);
        if (m) {
            var field = m[0].toLowerCase();
            console.log(m[0]);
            field = field.replace('{{','');
            field = field.replace('}}','');
            field = field.replace(/[a-zA-Z ]*\|[ ]*/g, ""); // for typed fields
            docContent[i].replace(m[0], fieldsMap.get(field));
            console.log("Replacing:"+m[0]+" with "+fieldsMap.get(field));
            docContent[i] = docContent[i].split(m[0]).join(fieldsMap.get(field));
        }
      } while (m);
    }
  }else{
    do {
      m = re.exec(docContent);
      if (m) {
          var field = m[0].toLowerCase();
          console.log(m[0]);
          field = field.replace('{{','');
          field = field.replace('}}','');
          field = field.replace(/[a-zA-Z ]*\|[ ]*/g, ""); // for typed fields
          docContent.replace(m[0], fieldsMap.get(field));
          console.log("Replacing:"+m[0]+" with "+fieldsMap.get(field));
          docContent = docContent.split(m[0]).join(fieldsMap.get(field));
      }
    } while (m);
  }
  return docContent;
}

/*************************************
  Search text for fields
**************************************/
function getFields(txt){
  var list_of_fields = [];
  txtEdit = txt;
  console.log(txtEdit);
  var re = /{{[a-zA-Z ]*\|[a-zA-Z0-9_ ]*}}/g; // For typed fields
  // var re = /{{[a-zA-Z0-9_ ]*}}/g; //  for non-typed arrays
  var m;
  do {
      m = re.exec(txt);
      if (m) {
          list_of_fields.push(m[0].toLowerCase());
      }
  } while (m);
  return list_of_fields;
}

/*************************************
  Text extraction functions
**************************************/

function getDocxFields(docContent){
  /*  
    The regular expression below will match the <w:t> element.
    The <w:t> element holds all of the textual content of a paragraph in
    a docx document.

    This method was used to get fields in strange situations
    The docx XML produced by microsoft word is different to the 
    docx XML produced by Google Docs. Word seems to split up sentences for some reason.
    To avoid this, this method combined all those splits so it can get the fields.
  */
  
  var re = /<w:p[\s\S]*?<\/w:p>/g; // match paragraph elements 
  var textRe = /<w:t(.)*?>(.|\n)*?(?=<\/w:t>)/g; // w:t elements contain the text
  var paragraphs = [];
  var m;
  do {
    m = re.exec(docContent);
    if (m) {
      var paragraphElement = m[0];
      var paragraph = "";
      do {
        paragraphText = textRe.exec(paragraphElement);
        if (paragraphText){
          // cleaning the paragraphText by removing the start of the element
          var p = paragraphText[0].replace(/<w:t(.)*?>/g, "");
          paragraph = paragraph+p;
        }
      } while (paragraphText);
      console.log(paragraph);
      paragraphs.push(paragraph);
    }
  } while (m);

  var allFields = [];
  for (var i=0;i<paragraphs.length;i++){
    // Now that paragraph elements combined, get fields.
    var fields = getFields(paragraphs[i]);
    allFields = allFields.concat(fields);
  }
  console.log(allFields);
  return allFields;
}

function odtTextContent(docContent){
  /*  
    The regular expression below will match the <text:p text:style-name=""> element.
  */
  var re = /(<text:p text:style-name="P(.)*?">(.|\n)*?(?=<\/text:p>))|<text:p text:style-name="P(.)*?" \/>/g;
  var paragraphs = [];
  var m;
  do {
      m = re.exec(docContent);
      if (m) {
        var paragraph = m[0];
        if (m[0] == /<text:p text:style-name="P(.)*?" \/>/g){
          paragraph = "\n";
          paragraphs.push(paragraph);
          // paragraphs.push(paragraphs);
        }else{
          // cleaning the paragraph by removing the start of the element
          paragraph = paragraph.replace(/<text:p text:style-name="P(.)*?">/g, "");
          paragraphs.push(paragraph);
        }
      }
  } while (m);
  console.log(paragraphs);
  return paragraphs;
}

function pptxTextContent(docContent){
  var re = /<a:t(.)*?>(.|\n)*?(?=<\/a:t>)/g;
  var paragraphs = [];
  var m;
  do {
      m = re.exec(docContent);
      if (m) {
          var paragraph = m[0];
          // cleaning the paragraph by removing the start of the element
          paragraph = paragraph.replace(/<a:t(.)*?>/g, "");
          paragraphs.push(paragraph);
      }
  } while (m);
  return paragraphs;
}


/*************************************
  PDF generation functions
*************************************/

function generateDocxPDF(fs, PDFDocument, docContent, relFile, images, filename, userId){
  var doc = new PDFDocument();
  var afterImage = false;
  // var paragraphs = docxTextContent(docContent);

  // NOTE: It seems to be quite complicated to add images within the middle of the text
  // So I will only add images above an below paragraphs for now.
  // Cannot wrap text around images.

  // doc.pipe(fs.createWriteStream('/tmp/file.pdf'));// # For saving locally
  // Saving PDF to a collection so it can be accessed from the client side and downloaded immediately
  var buffers = [];
  doc.on('data', buffers.push.bind(buffers));
  doc.on('end', Meteor.bindEnvironment( function () {
      var file = Buffer.concat(buffers);
      console.log(file);
      // This file will be deleted after it has been downloaded
      UserFiles.insert({ data:file, userID:userId, fileName:filename, deleteID:"TempFile" });
  }, function (e){
    throw e;
  }));
  
  var re = /<w:p[\s\S]*?<\/w:p>/g; // match paragraph elements 
  var textRe = /<w:t(.)*?>(.|\n)*?(?=<\/w:t>)/g;
  var match;
  var paragraphs = [];

  var image_re = /<pic:pic(.|\n)*?(?=<\/pic:pic>)/g;
  var currentY = 70;
  var m;

  do {
    match = re.exec(docContent); 

    // If we find a paragraph element
    if (match) {
      // Check if paragraph element contains just a pic element
      var isPic = image_re.exec(match[0]);
      if(isPic){
        // start image processing
        m = isPic;
        console.log("HEY: "+m[0]);
        var imageElement = m[0];
        imageElement = imageElement.replace(/<pic:pic(.)*?>/g, "");

        // NOTE: This is working, it is printing the images but not correctly.
        // It seems to be printing the images at a strange size.

        // This is repeated code from the generatepptx function
        var aOff_re = /<a:off x="[0-9]*" y="[0-9]*"\/>/g;
        var m1 = aOff_re.exec(imageElement);
        var x, y, width, height;
        if (m1){
          x = parseInt(String(m1[0]).match(/x="(.*?)"/g)[0].replace(/x="|"/g,""));
          y = parseInt(String(m1[0]).match(/y="(.*?)"/g)[0].replace(/y="|"/g,""));
        }

        // Get cx & cy measurements (width & height)
        var aExt_re = /<a:ext cx="[0-9]*" cy="[0-9]*"\/>/g;
        var m1 = aExt_re.exec(imageElement);
        if (m1){
          width = parseInt(String(m1[0]).match(/cx="(.*?)"/g)[0].replace(/cx="|"/g,""));
          height = parseInt(String(m1[0]).match(/cy="(.*?)"/g)[0].replace(/cy="|"/g,""));
        }

        var imageRel = String(imageElement).match(/r:embed="(.*?)"/g)[0].replace(/r:embed="|"/g, "");
        var regex = new RegExp(imageRel+"(.*?)/>");
        var stringWithImageName = String(relFile).match(regex);
        
        var imageName = String(stringWithImageName).match("media/(.*?)\"")[1];
        for (var n=0; n<images.length; n+=2){
          if(images[n+1] == imageName){
            // doc.image(new Buffer(images[n]), (x/9525)+MARGIN-LEFT, (y/9525)+MARGIN-TOP, {fit:[width/9525, height/9525]});
            doc.image(new Buffer(images[n]), (x/9526)+70, (y/9525)+70, {fit:[width/12700, height/12700]});
            afterImage = true;
            currentY += (height/12700)+20;
            console.log(height/9525);

          }
        }
        // finish image processing
      }else{
        console.log("Got here");
        var paragraphElement = match[0];
        var paragraph = "";
        do {
          paragraphText = textRe.exec(paragraphElement);
          if (paragraphText){
            // cleaning the paragraphText by removing the start of the element
            var p = paragraphText[0].replace(/<w:t(.)*?>/g, "");
            // console.log(paragraphText+"\n");
            paragraph = paragraph+p;
          }
        } while (paragraphText);
        // paragraphs.push(paragraph);
        if (afterImage){
          afterImage = false;
          doc.text(paragraph, 70, currentY);
          doc.moveDown();
        }else{
          doc.text(paragraph);
          doc.moveDown();
        }
      }
    }
  } while (match);

  doc.end();
}

function generateOdtPDF(fs, PDFDocument, paragraphs, userId, filename){
  /*
    Currently not handling images
  */

  var doc = new PDFDocument();
  // doc.pipe(fs.createWriteStream('/tmp/file.pdf'));

  // Saving PDF to a collection so it can be accessed from the client side and downloaded immediately
  var buffers = [];
  doc.on('data', buffers.push.bind(buffers));
  doc.on('end', Meteor.bindEnvironment( function () {
      var file = Buffer.concat(buffers);
      // This file will be deleted after it has been downloaded
      UserFiles.insert({ data:file, userID:userId, fileName:filename, deleteID:"TempFile" });
  }, function (e){
    throw e;
  }));

  for (i=0; i<paragraphs.length; i++){
    doc.moveDown();
    if (paragraphs[i].indexOf("<text:line-break/>") != -1){
      var newlines = paragraphs[i].split("<text:line-break/>");
      for(j=0; j<newlines.length; j++){
        doc.text(newlines[j],{'lineBreak':true});
      }
    }else{
      doc.text(paragraphs[i]);
    }
  }

  doc.end();
}

function generateOdsPDF(fs, PDFDocument, docContent, filename, userId){
  var doc = new PDFDocument();
  var row_re = /(<table:table-row(.)*?">(.|\n)*?(?=<\/table:table-row>))/g;
  var c = /<table:table-cell(.|\n)*?((\/>)|(<\/table:table\-cell>))/g; // cell values
  var stringIndex = /<text:p>(.|\n)*?(?=<\/text:p>)/g; // index values for sharedStrings 
  var m, cValue;
  var spreadsheet = []; // 2D array
  var rowCount = 0;

  do {
    var rowValues = [];
    m = row_re.exec(docContent);
    if (m) {
      var row = m[0];
      do {
        cValue = c.exec(row);
        if (cValue){
          var cell = cValue[0];
          if (cell.match("<text:p>")){
            console.log("Not Blank");
            var cellValue = cell.match("<text:p>(.|\n)*?(?=<\/text:p>)")[0].replace(/<text:p>|<\/v>/g,"");
            // var isRepeated = parseInt(cell.match("repeated=\"[0-9]*\"")[0].replace(/repeated=\"|"/g, ""));
            var isRepeated = cell.match("repeated=\"[0-9]*\"");
            if (isRepeated != null){
              var repeat =  parseInt(isRepeated[0].replace(/repeated=\"|"/g, ""));
              for (var k=0; k<repeat; k++){
                rowValues.push(cellValue);
              }
              continue;
            }
            rowValues.push(cellValue);
          }else{
            var blankCheck = cell.match("<table:table-cell table:number-columns-repeated=\"[0-9]*\"\/>");
            console.log("FOUND BLANK!");
            console.log(cell);
            if (blankCheck){ continue; }
            rowValues.push("");
          }
        }
      } while(cValue);
      spreadsheet.push(rowValues);
      rowCount++;
    }
  } while (m);

  // Popping twice to remove the row elements added by ods
  // The removed elements just specify that there are 1000+ duplicates of the same row
  // Simply telling us that the rest are blank
  spreadsheet.pop();
  spreadsheet.pop();
  console.log("SPREADSHEET");
  console.log(spreadsheet);

  // doc.pipe(fs.createWriteStream('/tmp/file.pdf'));// # write to PDF
  // Saving PDF to a collection so it can be accessed from the client side and downloaded immediately
  var buffers = [];
  doc.on('data', buffers.push.bind(buffers));
  doc.on('end', Meteor.bindEnvironment( function () {
      var file = Buffer.concat(buffers);
      console.log(file);
      // This file will be deleted after it has been downloaded
      UserFiles.insert({ data:file, userID:userId, fileName:filename, deleteID:"TempFile" });
  }, function (e){
    throw e;
  }));

  doc.fontSize(10); // Form can be destroyed if font changes
  buildSpreadsheetPDF(doc, false, spreadsheet, null);

  doc.end();

}

function generateXlsxPDF(fs, PDFDocument, docSheet, sharedStrings, filename, userId){
  
  var doc = new PDFDocument();

  // Get the col width and row height
  var defaultColWidth = parseFloat(String(docSheet).match("defaultColWidth=\"(.*?)\"")[0].replace(/defaultColWidth="|"/g,""));
  var defaultRowHeight = parseFloat(String(docSheet).match("defaultRowHeight=\"(.*?)\"")[0].replace(/defaultRowHeight="|"/g,""));
  console.log("Default Col Width: "+String(defaultColWidth));
  console.log("Default Row Width: "+String(defaultRowHeight));

  // doc.pipe(fs.createWriteStream('/tmp/file.pdf'));// for saving locally 
  // Saving PDF to a collection so it can be accessed from the client side and downloaded immediately
  var buffers = [];
  doc.on('data', buffers.push.bind(buffers));
  doc.on('end', Meteor.bindEnvironment( function () {
      var file = Buffer.concat(buffers);
      console.log(file);
      // This file will be deleted after it has been downloaded
      UserFiles.insert({ data:file, userID:userId, fileName:filename, deleteID:"TempFile" });
  }, function (e){
    throw e;
  }));
  doc.fontSize(10);

  // This will build the list of strings in the spreadsheet
  // using the sharedStrings file
  var m;
  var allSharedStrings = [];
  var re = /<t>(.|\n)*?(?=<\/t>)/g;
  do {
    m = re.exec(sharedStrings);
    if (m) {
        var s = m[0];
        s = s.replace(/<t(.)*?>/g, "");
        allSharedStrings.push(s);
    }
  } while (m);

  // re will match row elements
  // c will match the cell elements within those rows
  // stringIndex will match the index values inside the <v> elements of the cell elements
  re = /<row(.*?)>(.|\n)*?(?=<\/row>)/g;
  var c = /(<c r=(.*) s=(.*) \/>)|(<c r=(.*) s=(.*) t=(.*)>(.|\n)*?(?=<\/c>))/g; // cell values
  var stringIndex = /<v>(.|\n)*?(?=<\/v>)/g; // index values for sharedStrings 
  var indexValueMatch;
  var rows = []; // contains lists of cell values for each row
  var cValue;
  var spreadsheet = []; // 2D array
  var rowCount = 0;

  do {
    var rowValues = [];
    m = re.exec(docSheet);
    if (m) {
      var row = m[0];
      do{
        cValue = c.exec(row);
        if (cValue){
          var cell = cValue[0];
          cell = cell.split("<c");
          var rowArr = [];
          for (var k=1; k<cell.length; k++){ // counting the columns here (k=1 because split function adds blank to index 0)
            if (cell[k].match("<v>(.*)</v>")){
              console.log("Not Blank");
              console.log(cell[k]+"\n");
              var cellValue = cell[k].match("<v>(.*)</v>")[0].replace(/<v>|<\/v>/g,"");
              rowValues.push(cellValue);
            }else{
              console.log("FOUND BLANK!");
              console.log(cell[k]+"\n");
              rowValues.push(""); // push just blank
              // rowValues.push("");
            }
          }
          spreadsheet.push(rowValues);
          rowCount++;
        }
      } while(cValue);
    }
  } while(m);
  
  // At this point we have a 2d array representation of the spreadsheet
  // Each cell value is an index value to be used with the allSharedStrings list
  console.log(spreadsheet);
  // NOTE: can only handle square matrices, so its assumed that there will be 
  // the same number of columns as there are rows (thus no out of bounds issues with spreadsheet array)
  buildSpreadsheetPDF(doc, true, spreadsheet, allSharedStrings);
  doc.end();
}
  
function generatePptxPDF(fs, PDFDocument, slides, images, relFiles, filename, userId){

  var doc = new PDFDocument({
            layout : 'landscape',
            size: [5143500/9525, 9144000/9525]
  });

  var isFirstSlide = true;
  /*
    NOTE: Regarding font sizes
    Below is an answer given by a member of the powerpoint team:
    http://openxmldeveloper.org/discussions/formats/f/15/t/1301.aspx
       Regarding the size of the text for a given shape, it is also defined in 
       slideMaster1.xml when nowhere else overrides it. For example, in the 
       slide1.xml, the title's font size is 44. It's defined in slideMaster1.xml: 
       a:defRPr sz="4400"
  */

  // In this case default size for Title:28 and Body:18
  // doc.fontSize(18);
  // doc.pipe(fs.createWriteStream('/tmp/file.pdf'));// for saving locally

  // Saving PDF to a collection so it can be accessed from the client side and downloaded immediately
  var buffers = [];
  doc.on('data', buffers.push.bind(buffers));
  doc.on('end', Meteor.bindEnvironment( function () {
      var file = Buffer.concat(buffers);
      console.log(file);
      // This file will be deleted after it has been downloaded
      UserFiles.insert({ data:file, userID:userId, fileName:filename, deleteID:"TempFile" });
  }, function (e){
    throw e;
  }));

  // each slide will be a new pdf page
  for (var i=0; i<slides.length; i++){

    if (isFirstSlide){
      isFirstSlide = false;
    }else{
      doc.addPage();
    }

    var allSlideElements = [];
    var slide =  slides[i];
    var re = /<p:sp>(.|\n)*?(?=<\/p:sp>)/g;
    var image_re = /<p:pic>(.|\n)*?(?=<\/p:pic>)/g;
    var m;
    // Typically the first <p:sp> element is the title
    // The next <p:sp> element usually contains the text content(paragraphs, bullets etc..)
    do {
      m = re.exec(slide);
      if (m) {
        var slideElement = m[0];
        // cleaning the paragraph by removing the start of the element
        slideElement = slideElement.replace(/<p:sp(.)*?>/g, "");
        // console.log(paragraph+"\n");
        allSlideElements.push(slideElement);
      }
    } while (m);

    // Finding image elements
    do {
      m = image_re.exec(slide);
      if (m) {
        var imageElement = m[0];
        imageElement = imageElement.replace(/<p:pic(.)*?>/g, "");
        allSlideElements.push(imageElement);
      }
    } while (m);

    for (var j=0; j<allSlideElements.length; j++){
      console.log("WORKING ON SLIDE "+(i+1));
      // Get x & y coordinates
      var re = /<a:off x="[0-9]*" y="[0-9]*"\/>/g;
      var m = re.exec(allSlideElements[j]);
      var x, y, width, height;
      if (m){
        x = parseInt(String(m[0]).match(/x="(.*?)"/g)[0].replace(/x="|"/g,""));
        y = parseInt(String(m[0]).match(/y="(.*?)"/g)[0].replace(/y="|"/g,""));
      }

      // Get cx & cy measurements (width & height)
      var re = /<a:ext cx="[0-9]*" cy="[0-9]*"\/>/g;
      var m = re.exec(allSlideElements[j]);
      if (m){
        width = parseInt(String(m[0]).match(/cx="(.*?)"/g)[0].replace(/cx="|"/g,""));
        height = parseInt(String(m[0]).match(/cy="(.*?)"/g)[0].replace(/cy="|"/g,""));
      }

      // Checking here to see if we are dealing with an image element
      var re = /<p:blipFill>/g;
      var m = re.exec(allSlideElements[j]);
      if (m){
        var imageRel = String(allSlideElements[j]).match(/r:embed="(.*?)"/g)[0].replace(/r:embed="|"/g, "");
        console.log("#### FOUND AN IMAGE ELEMENT ####");
        console.log(imageRel);

        var regex = new RegExp(imageRel+"(.*?)/>");
        var currentRelFile = relFiles[i];
        var stringWithImageName = String(currentRelFile).match(regex);
        console.log("stringWithImageName below:");
        console.log(stringWithImageName[0]);

        var imageName = String(stringWithImageName).match("../media/(.*?)\"")[1];
        console.log("imageName: "+imageName);
        for (var n=0; n<images.length; n+=2){
          // console.log(images[n+1]);
          if(images[n+1] == imageName){
            // placing the image using the x,y,width,height variable
            doc.image(new Buffer(images[n]), x/9525, y/9525, {fit:[width/9525, height/9525]});
          }
        }
        continue; // The rest of the code has nothing to do with pic elements, so continue to next element
      }

      // Checking type of element to get fontSize
      var elementType = allSlideElements[j].match(/<p:ph(.*?)type="(.*?)"/g)[0].replace(/<p:ph(.*?)type="|"/g, "");
      console.log("ELEMENT TYPE: "+elementType);

      var lines = pptxTextContent(allSlideElements[j]);
      console.log("Size of lines:"+lines.length);
      // would need to check here if text is of type title to adjust fontSize

      for (var k=0; k<lines.length; k++){
        // Dividing EMU value by 9525 to get pixel value
        // doc.text(lines[k], (x/9525), (y/9525)+(k*38));
        if (elementType=="ctrTitle"){
          // ctrTitle is a large title positioned in the middle of the slide
          doc.fontSize(38);
          doc.text(lines[k], (x/9525), (height/9525), {align:'center'});
          doc.moveDown();
        }else{
          // NOTE: Need to handle sub titles
          if (elementType=="title"){
            doc.fontSize(38);
            doc.text(lines[k], (x/9525), (y/9525));
            doc.moveDown();
          }else{
            doc.fontSize(18);
            if(k==0){
              doc.text(lines[k], (x/9525), (y/9525));
              doc.moveDown();
            }else{
              doc.text(lines[k]);
              doc.moveDown();
            }
          }
        }
      } // lines.length loop
    } // allSlideElements.length loop
  } // slides.length loop
  doc.end();
  /*
    Interesting information.
    To get PT divide by 12700
    To get pixels divide by 9525
    */
}

/*************************************
  PDF generation helper functions
*************************************/

function buildSpreadsheetPDF(doc, isXlsx, spreadsheet, allSharedStrings){
  var defaultRowHeight = 15.74; // should really get this from the document
  var currentX = 70; // margin value
  var currentY = 70; // margin value
  for (var colCount=0; colCount<spreadsheet.length; colCount++){ 
    var columnWidth = 0;
    // getting the best width for the column
    for (var rowCount=0; rowCount<spreadsheet.length; rowCount++){ 
      // if (spreadsheet[rowCount][colCount] == ""){ 
      if (spreadsheet[rowCount][colCount] == ""){ 
        console.log("BLANK");
        continue; 
      }
      if(isXlsx){
        var cellStringValue = allSharedStrings[parseInt(spreadsheet[rowCount][colCount])];  
      }else{
        var cellStringValue = spreadsheet[rowCount][colCount];
      }
      if (cellStringValue.length > columnWidth){
        columnWidth = cellStringValue.length;
      }
      console.log(cellStringValue);
    }

    for (var rowCount=0; rowCount<spreadsheet.length; rowCount++){
      doc.rect(currentX, currentY, columnWidth*5, defaultRowHeight).stroke();
      if (isXlsx){
        if (allSharedStrings[parseInt(spreadsheet[rowCount][colCount])] !== undefined){
          doc.text(allSharedStrings[parseInt(spreadsheet[rowCount][colCount])], currentX+3, currentY+3); // 3 is the padding
         }else{
           doc.text("", currentX, currentY);
         }
      }else{
        // if (spreadsheet[rowCount][colCount] != ""){
        if (spreadsheet[rowCount][colCount] != ""){
          doc.text(spreadsheet[rowCount][colCount], currentX+3, currentY+3); // 3 is padding
        }else{
          doc.text("", currentX, currentY);
        }
      }
      currentY += defaultRowHeight;
    }
    currentY = 70;
    currentX += columnWidth*5; // 5px per character

    // This if statement is very important. It will return if no more cells
    // can fit on the page. Without this there will be terrible runtime errors
    if (currentX>500) return;
  }
}








